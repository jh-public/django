#!/usr/bin/env bash
set -eu
tag=${1:?}
git tag  --delete $tag || true
git push --delete origin $tag || true
git tag $tag
git push origin $tag

