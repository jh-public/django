FROM nginx/unit:1.29.0-python3.11

COPY packages.txt requirements.txt /

RUN date \
    && mkdir -p /app \
    && useradd user

RUN \
       apt-get update \
    && apt-get install -y --no-install-recommends $(cat /packages.txt)

RUN \
       pip3 install --upgrade pip setuptools \
    && pip3 install -r /requirements.txt

WORKDIR /app
