#!/usr/bin/env bash
set -eu

TAG_LATEST=false

while getopts 'hl' opt; do
    case $opt in
        h) help;;
        l) TAG_LATEST=true;;
    esac
done
shift $((OPTIND-1))


help() {
    echo "usage: $(basename $0) VERSION"
    exit 0
}

mkreadme() {
    local ver="${1:?}"
    filename="README_${ver}.md"
    echo > "$filename" "
## Version $ver
### Usage
    cd django-project-directory
    docker run -it -p 8000:8000 -v \$PWD:/app huanjason/django
### Packages
"
    docker run huanjason/${name}:${ver} pip freeze | ts -- "- " >> "$filename"
}

main() {
    readonly version=${1:?$(help)}
    readonly name="$( basename $PWD )"

    set -x
    time docker build -t huanjason/${name}:${version} .
    set +x

    if $TAG_LATEST; then
        docker tag huanjason/${name}:${version} huanjason/${name}:latest
    fi

    mkreadme $version
}

main "$@"
